#include <cmath>
#include <stdexcept>
#include "s21_matrix_oop.h"

bool S21Matrix::eq_matrix(const S21Matrix& other) {
    return _matrix == other._matrix && is_matrix_identical_sizes(other);
}

void S21Matrix::sum_sub_matrix(const S21Matrix& other, int sign) {
    if (is_matrix_identical_sizes(other)) {
        for (int i = 0; i < _rows; i++) {
            for  (int j = 0; j < _cols; j++) {
                int pos = get_pos(i, j);
                _matrix[pos] = _matrix[pos] + sign * other._matrix[pos];
            }
        }
    } else {
        throw std::logic_error("Matrices has different sizes.");
    }
}


void S21Matrix::sum_matrix(const S21Matrix& other) {
    sum_sub_matrix(other, 1);
}

void S21Matrix::sub_matrix(const S21Matrix& other) {
    sum_sub_matrix(other, -1);
}

void S21Matrix::mul_number(const double num) {
    bool number_is_ok = !std::isnan(num) && !std::isinf(num);

    if (number_is_ok) {
        for (int i = 0; i < _rows; i++) {
            for  (int j = 0; j < _cols; j++) {
                int pos = get_pos(i, j);
                _matrix[pos] = _matrix[pos] * num;
            }
        }
    }
}

void S21Matrix::mul_matrix(const S21Matrix& other) {
    if (_cols != other._rows) {
        throw std::logic_error("Number Columns of Matrix different with rows of other matrix.");
    }

    S21Matrix mulMatrix(_rows, other._cols);
    for (int i = 0; i < _rows; i++) {
        for  (int j = 0; j < other._cols; j++) {
            double summ = 0.;

            for (int k = 0; k < _cols; k++) {
                int pos_i_k = get_pos(i, k);
                int pos_k_j = other.get_pos(k, j);
                summ += _matrix[pos_i_k] * other._matrix[pos_k_j];
            }
            mulMatrix._matrix.push_back(summ);
        }
    }
    (*this) = mulMatrix;
}

S21Matrix S21Matrix::transpose() {
    S21Matrix ret(_cols, _rows);

    for  (int j = 0; j < _cols; j++) {
        for (int i = 0; i < _rows; i++) {
            ret._matrix.push_back((*this)(i, j));
        }
    }
    return ret;
}

S21Matrix S21Matrix::calc_complements() {
    if (!is_square_matrix()) {
        throw std::logic_error("Matrix not square.");
    }

    S21Matrix ret(_rows, _cols);
    if (_rows == 1) {
        ret._matrix.push_back(1);
    } else {
        for (int i = 0; i < _rows; i++) {
            for  (int j = 0; j < _cols; j++) {
                S21Matrix tmp_M = matrix_without_row_col(i, j);
                ret._matrix.push_back(std::pow(-1, i+j) * tmp_M.determinant());
            }
        }
    }
    return ret;
}

double S21Matrix::determinant() {
    if (!is_square_matrix()) {
        throw std::logic_error("Matrix not square.");
    }

    double ret = 0.;

    if (_rows == 1) {
        ret = _matrix[0];
    } else if (_rows == 2) {
        ret = (*this)(0, 0) * (*this)(1, 1) - (*this)(1, 0) * (*this)(0, 1);
    } else {
        for (int i = 0; i < _rows; i++) {
            int j = 0;
            int pos_i_j = get_pos(i, j);
            S21Matrix tmp_M = matrix_without_row_col(i, j);
            ret += pow(-1, i+j) * _matrix[pos_i_j] * tmp_M.determinant();
        }
    }
    return ret;
}

S21Matrix S21Matrix::inverse_matrix() {
    if (!is_square_matrix()) {
        throw std::logic_error("Matrix not square.");
    }

    double det = determinant();
    if (std::fabs(det) == 0) {
        throw std::logic_error("Determinant is zero.");
    }

    S21Matrix cmpl(calc_complements());
    S21Matrix ret(cmpl.transpose());
    ret.mul_number(1/det);
    return ret;
}
