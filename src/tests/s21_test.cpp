#include "../s21_matrix_oop.h"
#include <fstream>
#include <gtest/gtest.h>
#include <vector>


/**
 * @brief Macros for printing
 * #name - is system defined macros(?) who do print variable name
 */
#define PRINT_MTR(name) #name << ":\n" << testing::PrintToString(name)

#define N_FILES 24
std::vector<std::string> files = {
    "tests/matrix/1.mtrx",                          //  0
    "tests/matrix/1x2.mtrx",                        //  1
    "tests/matrix/2x2_0.mtrx",                      //  2
    "tests/matrix/2x2_i.mtrx",                      //  3
    "tests/matrix/2x2.mtrx",                        //  4
    "tests/matrix/3x3_7_123.mtrx",                  //  5
    "tests/matrix/3x3_8_123042.mtrx",               //  6
    "tests/matrix/3x3_9_for_inverse.mtrx",          //  7
    "tests/matrix/3x3_10_sub_123_123042.mtrx",      //  8
    "tests/matrix/3x3_11_sum_123_123042.mtrx",      //  9
    "tests/matrix/3x3_12_mult_number_123.mtrx",     // 10
    "tests/matrix/13 2x3.mtrx",                     // 11
    "tests/matrix/14 3x4.mtrx",                     // 12
    "tests/matrix/15 2x4 mult result.mtrx",         // 13
    "tests/matrix/16 3x3_mult_resukt.mtrx",         // 14
    "tests/matrix/3x3.mtrx",                        // 15
    "tests/matrix/4x4.mtrx",                        // 16
    "tests/matrix/5x5.mtrx",                        // 17
    "tests/matrix/1x2_transpose_to_2_1.mtrx",       // 18
    "tests/matrix/14 3x4_transpose.mtrx",           // 19
    "tests/matrix/22 4x4 complements.mtrx",         // 20
    "tests/matrix/23 1_1_1.mtrx",                   // 21
    "tests/matrix/24 3x3_9_for_inverse.mtrx",       // 22
    "tests/matrix/25 3x3 zero_matrix.mtrx"          // 23
    };

S21Matrix ms[N_FILES];

void read_all_files() {
    for (int i = 0; i < N_FILES; i++) {
        ms[i] = S21Matrix(files[i]);
    }
}

int *read_int_array(const char *fname, int count_int) {
    std::ifstream infile(fname);
    static int ret[500];
    if (infile.is_open()) {
        for (int i = 0; i < count_int; i++) {
            infile >> ret[i];
        }
        infile.close();
    }
    return ret;
}

TEST(test_s21_matrix, other_utils) {
    EXPECT_ANY_THROW({
        S21Matrix("tests/matrix/0_incorrect.mtrx");
    });
    EXPECT_ANY_THROW({
        S21Matrix("tests/matrix/not_found_file.mtrx");
    });
    EXPECT_ANY_THROW({
        S21Matrix("tests/matrix/0_incorrect2.mtrx");
    });
    EXPECT_ANY_THROW({
        S21Matrix ret(-5, 5);
    });

    // check Def constructor with 3x3 size working ok
    S21Matrix ret;
    EXPECT_EQ(ret.getRows(), 3);
    EXPECT_EQ(ret.getColumns(), 3);

    std::vector<S21Matrix> v;

    v.push_back(S21Matrix());

    EXPECT_ANY_THROW({
        ret(-5, 5);
    });

    ret.setRows(10);
    ret.setColumns(10);
}

TEST(test_s21_matrix, eq_matrix) {
    int *res = read_int_array("tests/matrix/test_s21_eq_matrix.result", N_FILES == 1 ? 1: N_FILES * N_FILES);

    int matric_to_work = 7;

    for (int i = 0; i < matric_to_work; i++) {
        for (int j = 0; j < matric_to_work; j++) {
            bool tmp = ms[i].eq_matrix(ms[j]);
            EXPECT_EQ((bool)res[matric_to_work*i+j], tmp) <<
            "Test failed. i=" << i <<
            " j=" << j <<
            " res[" << matric_to_work*i+j << "]=" << res[matric_to_work * i + j] <<
            " tmp=" << tmp;
        }
    }
}


TEST(test_s21_matrix, determinant) {
    double r0 = ms[0].determinant();
    EXPECT_ANY_THROW({
        ms[1].determinant();
    });
    double r2 = ms[2].determinant();
    double r3 = ms[3].determinant();
    double r4 = ms[4].determinant();
    double r5 = ms[5].determinant();
    double r6 = ms[6].determinant();
    double r7 = ms[7].determinant();
    double r8 = ms[8].determinant();
    double r15 = ms[15].determinant();
    double r17 = ms[17].determinant();
    double r23 = ms[23].determinant();

    EXPECT_EQ(r0, 9);

    EXPECT_EQ(r2, 0);
    EXPECT_EQ(r3, 1);
    EXPECT_EQ(r4, -2);
    ASSERT_DOUBLE_EQ(r5, 0);
    EXPECT_EQ(r6, -20);
    EXPECT_EQ(r7, -1);
    EXPECT_EQ(r8, 64);
    EXPECT_EQ(r15, 54);
    EXPECT_EQ(r17, -672015302);
    EXPECT_EQ(r23, 0);
}


TEST(test_s21_matrix, calc_complements) {
    S21Matrix r3 = ms[0].calc_complements();
    EXPECT_ANY_THROW({
        ms[1].calc_complements();
    });
    S21Matrix r5 = ms[16].calc_complements();
    S21Matrix r6 = ms[23].calc_complements();

    EXPECT_EQ(r3 == ms[21], true) << PRINT_MTR(ms[21]) << PRINT_MTR(r3) << PRINT_MTR(ms[16]);
    EXPECT_EQ(r5 == ms[20], true);
    EXPECT_EQ(r6 == ms[23], true);
}


TEST(test_s21_matrix, inverse_matrix) {
    //  *** Determinant is zero.
    EXPECT_ANY_THROW({
        ms[1].inverse_matrix();
    });
    EXPECT_ANY_THROW({
        ms[23].inverse_matrix();
    });

    EXPECT_ANY_THROW({
        ms[5].inverse_matrix();
    });

    S21Matrix r3 = ms[3].inverse_matrix();
    S21Matrix r5 = ms[7].inverse_matrix();

    EXPECT_EQ(r3.eq_matrix(ms[3]), true) << PRINT_MTR(ms[3]) << PRINT_MTR(r3) << PRINT_MTR(ms[3]);
    EXPECT_EQ(r5.eq_matrix(ms[22]), true) << PRINT_MTR(ms[7]) << PRINT_MTR(r5) << PRINT_MTR(ms[22]);
}

TEST(test_s21_matrix, sum_matrix) {
    EXPECT_ANY_THROW({
        ms[4].sum_matrix(ms[5]);
    });
    EXPECT_ANY_THROW({
        ms[4] + ms[5];
    });

    S21Matrix r5 = ms[5];
    S21Matrix r5_1 = ms[5];
    r5.sum_matrix(ms[6]);
    S21Matrix r23 = ms[23];
    r23.sum_matrix(ms[23]);

    r5_1 + ms[6];
    r5_1 += ms[6];

    EXPECT_EQ(r5 == ms[9], true) << PRINT_MTR(r5) << PRINT_MTR(ms[9]);
    EXPECT_EQ(r23 == ms[23], true) << PRINT_MTR(r23) << PRINT_MTR(ms[23]);

    EXPECT_EQ(r5_1 == ms[9], true) << PRINT_MTR(r5_1) << PRINT_MTR(ms[9]);
}


TEST(test_s21_matrix, sub_matrix) {
    EXPECT_ANY_THROW({
        ms[4].sub_matrix(ms[5]);
    });
    EXPECT_ANY_THROW({
        ms[4] - ms[5];
    });
    S21Matrix r5(ms[5]);
    S21Matrix r5_1 = ms[5];
    r5.sub_matrix(ms[6]);
    S21Matrix r23(ms[23]);
    r23.sub_matrix(ms[23]);

    r5_1 - ms[6];
    r5_1 -= ms[6];

    EXPECT_EQ(r5.eq_matrix(ms[8]), true) <<
                PRINT_MTR(r5) <<
                PRINT_MTR(ms[5]) <<
                PRINT_MTR(ms[6]) <<
                PRINT_MTR(ms[8]);

    EXPECT_EQ(r23.eq_matrix(ms[23]), true) <<
                PRINT_MTR(r23) <<
                PRINT_MTR(ms[23]);

    EXPECT_EQ(r5_1 == ms[8], true) <<
                PRINT_MTR(r5_1) <<
                PRINT_MTR(ms[5]) <<
                PRINT_MTR(ms[6]) <<
                PRINT_MTR(ms[8]);
}


TEST(test_s21_matrix, mult_number) {
    S21Matrix r2(ms[23]);
    S21Matrix r3(ms[5]);

    S21Matrix r2_1 = ms[23];
    S21Matrix r3_1 = ms[5];

    r2.mul_number(-0.01);
    r3.mul_number(-0.01);

    r2_1 *= -0.01;
    r2_1 * -0.01;

    r3_1 *= -0.01;
    r3_1 * -0.01;

    EXPECT_EQ(r2.eq_matrix(ms[23]), true) << PRINT_MTR(r2) << PRINT_MTR(ms[23]);
    EXPECT_EQ(r3.eq_matrix(ms[10]), true) << PRINT_MTR(r3) << PRINT_MTR(ms[10]);

    EXPECT_EQ(r2_1 == ms[23], true) << PRINT_MTR(r2_1) << PRINT_MTR(ms[23]);
    EXPECT_EQ(r3_1 == ms[10], true) << PRINT_MTR(r3_1) << PRINT_MTR(ms[10]);
}

TEST(test_s21_matrix, mult_matrix) {
    S21Matrix r5(ms[5]);
    S21Matrix r11(ms[11]);
    S21Matrix r23(ms[23]);

    S21Matrix r5_1 = ms[5];

    EXPECT_ANY_THROW({
        ms[4].mul_matrix(ms[5]);
    });

    EXPECT_ANY_THROW({
        ms[4] * ms[5];
    });

    r5.mul_matrix(ms[6]);
    r11.mul_matrix(ms[12]);
    r23.mul_matrix(ms[23]);

    r5_1 *= ms[6];

    EXPECT_EQ(r5.eq_matrix(ms[14]), true) << PRINT_MTR(r5) <<
                                             PRINT_MTR(ms[5]) <<
                                             PRINT_MTR(ms[6]) <<
                                             PRINT_MTR(ms[14]);

    EXPECT_EQ(r11.eq_matrix(ms[13]), true) << PRINT_MTR(r11) <<
                                              PRINT_MTR(ms[11]) <<
                                              PRINT_MTR(ms[12]) <<
                                              PRINT_MTR(ms[13]);

    EXPECT_EQ(r23.eq_matrix(ms[23]), true) << PRINT_MTR(r23) <<
                                              PRINT_MTR(ms[23]);

    EXPECT_EQ(r5_1 == ms[14], true) << PRINT_MTR(r5_1) <<
                                       PRINT_MTR(ms[14]);
}


TEST(test_s21_matrix, transpose) {
    S21Matrix r3 = ms[1].transpose();
    S21Matrix r4 = ms[12].transpose();
    S21Matrix r5 = ms[23].transpose();


    EXPECT_EQ(r3.eq_matrix(ms[18]), true) << PRINT_MTR(r3) << PRINT_MTR(ms[18]);

    EXPECT_EQ(r4.eq_matrix(ms[19]), true) << PRINT_MTR(ms[12])<< PRINT_MTR(r4) << PRINT_MTR(ms[19]);
    EXPECT_EQ(r5.eq_matrix(ms[23]), true) << PRINT_MTR(r5) << PRINT_MTR(ms[23]);
}



int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    read_all_files();
    return RUN_ALL_TESTS();
}
