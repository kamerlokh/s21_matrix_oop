#include "s21_matrix_oop.h"


int S21Matrix::getRows() const {
    return _rows;
}

int S21Matrix::getColumns() const {
    return _cols;
}

void S21Matrix::setRows(int r) {
    _rows = r;
    _matrix.resize(_rows * _cols, 0);
}

void S21Matrix::setColumns(int c) {
    _cols = c;
    _matrix.resize(_rows * _cols, 0);
}
