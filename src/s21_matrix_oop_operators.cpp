#include <stdexcept>

#include "s21_matrix_oop.h"


bool S21Matrix::operator==(const S21Matrix& other) {
    return (*this).eq_matrix(other);
}

//  *** Assignment of values from one matrix to another one
void S21Matrix::operator=(const S21Matrix& other) {
    _rows = other._rows;
    _cols = other._cols;
    _matrix = other._matrix;
}

//  *** Addition of two matrices
//      Except: different matrix dimensions
S21Matrix S21Matrix::operator+(const S21Matrix& other) {
    S21Matrix ret(*this);
    ret.sum_matrix(other);
    return ret;
}

//  *** Addition assignment (sum_matrix)
//      Except: different matrix dimensions
void S21Matrix::operator+=(const S21Matrix& other) {
    sum_matrix(other);
}

//  *** Subtraction of one matrix from another
//      Except: different matrix dimensions
S21Matrix S21Matrix::operator-(const S21Matrix& other) {
    S21Matrix ret(*this);
    ret.sub_matrix(other);
    return ret;
}

//  *** Difference assignment (sub_matrix)
//      Except: different matrix dimensions
void S21Matrix::operator-=(const S21Matrix& other) {
    sub_matrix(other);
}

//  *** Matrix multiplication and matrix multiplication by a number
//      Except: the number of columns of the first matrix does not equal
//              the number of rows of the second matrix
S21Matrix S21Matrix::operator*(const S21Matrix& other) {
    S21Matrix ret(*this);
    ret.mul_matrix(other);
    return ret;
}

S21Matrix S21Matrix::operator*(const double num) {
    S21Matrix ret(*this);
    ret.mul_number(num);
    return ret;
}

//  *** Multiplication assignment (mul_matrix/mul_number)
//      Except: the number of columns of the first matrix does not equal
//              the number of rows of the second matrix
void S21Matrix::operator*=(const S21Matrix& other) {
    (*this) = (*this) * other;
}

void S21Matrix::operator*=(const double num) {
    (*this) = (*this) * num;
}

//  *** Indexation by matrix elements (row, column)
//      Except: index is outside the matrix
double S21Matrix::operator()(int row, int col) const {
    bool row_is_ok = row >= 0 && row < _rows;
    bool col_is_ok = col >= 0 && col < _cols;

    if (row_is_ok && col_is_ok) {
        int pos = get_pos(row, col);
        return _matrix[pos];
    }

    throw std::logic_error("Index is outside the matrix.");
}
