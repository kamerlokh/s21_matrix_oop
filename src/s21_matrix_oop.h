#ifndef SRC_S21_MATRIX_OOP_H_
#define SRC_S21_MATRIX_OOP_H_

#include <vector>
#include <iostream>

class S21Matrix {
 private:
        //  *** Rows and columns
        int _rows{-1}, _cols{-1};
        //  *** Pointer to the memory where the matrix is allocated
        //    double **_matrix {nullptr};
        std::vector<double> _matrix;

        /**
         * Helpers methods
         */
        //  *** Sum and Sub matrix
        void sum_sub_matrix(const S21Matrix& other, int sign);
        //  *** Check if matrix identical sizes
        bool is_matrix_identical_sizes(const S21Matrix& other) const;
        int get_pos(int i, int j) const;
       int get_pos(int i, int j, int count_rows) const;

        //  *** Check if matrix correct
        bool is_matrix_correct() const;
        //  *** Check if matrix is square
        bool is_square_matrix() const;
        //  *** Get matrix withot burned row and cols
        S21Matrix matrix_without_row_col(int i, int j);
        //  *** Realloc memory for matrix
        void matrix_realloc(int rows, int cols);
        //  *** Copy data from other matrix (without check sizes)
        void copy_from(const S21Matrix& other);

 public:
        explicit S21Matrix(std::string file_name);   // Read Matrix From File

        /** 
         * Constructors & Destructors
         */

        //  *** Default constructor
        S21Matrix();
        //  *** Parametrized constructor with number of rows and columns
        S21Matrix(int rows, int cols);
        //  *** Copy constructor
        S21Matrix(const S21Matrix& other);
        //  *** Move constructor
        S21Matrix(S21Matrix&& other);
        //  *** Destructor
        ~S21Matrix();

        /**
         * Accessors & Mutators
         */
        //  *** Rows accessor
        int getRows() const;
        //  *** Columns accessor
        int getColumns() const;
        //  *** Rows mutator
        void setRows(int rows);
        //  *** Columns mutator
        void setColumns(int cols);

        /** 
         * Matrix operations
         */

        //  *** +Checks matrices for equality with each other
        bool        eq_matrix(const S21Matrix& other);

        //  *** +Adds the second matrix to the current one
        //      Except: different matrix dimensions
        void        sum_matrix(const S21Matrix& other);

        // *** +Subtracts another matrix from the current one
        //     Except: different matrix dimensions
        void        sub_matrix(const S21Matrix& other);

        //  *** +Multiplies the current matrix by a number
        void        mul_number(const double num);

        //  *** +Multiplies the current matrix by the second matrix
        //      Except: the number of columns of the first matrix is not equal
        //              to the number of rows of the second matrix
        void        mul_matrix(const S21Matrix& other);

        //  *** +Creates a new transposed matrix from the current one and returns it
        S21Matrix   transpose();

        //  *** +Calculates the algebraic addition matrix of the current one and
        //       returns it Except: the matrix is not square
        S21Matrix   calc_complements();

        //  *** +Calculates and returns the determinant of the current matrix
        //      Except: the matrix is not square
        double      determinant();

        //  *** Calculates and returns the inverse matrix
        //      Except: matrix determinant is 0
        S21Matrix   inverse_matrix();

        // Operators
        //  *** Checks for matrices equality (eq_matrix)
        bool operator==(const S21Matrix& other);

        //  *** Assignment of values from one matrix to another one
        void operator=(const S21Matrix& other);

        //  *** Addition of two matrices
        //      Except: different matrix dimensions
        S21Matrix operator+(const S21Matrix& other);

        //  *** Addition assignment (sum_matrix)
        //      Except: different matrix dimensions
        void operator+=(const S21Matrix& other);

        //  *** Subtraction of one matrix from another
        //      Except: different matrix dimensions
        S21Matrix operator-(const S21Matrix& other);

        //  *** Difference assignment (sub_matrix)
        //      Except: different matrix dimensions
        void operator-=(const S21Matrix& other);

        //  *** Matrix multiplication and matrix multiplication by a number
        //      Except: the number of columns of the first matrix does not equal
        //              the number of rows of the second matrix
        S21Matrix operator*(const S21Matrix& other);
        S21Matrix operator*(const double num);

        //  *** Multiplication assignment (mul_matrix/mul_number)
        //      Except: the number of columns of the first matrix does not equal
        //              the number of rows of the second matrix
        void operator*=(const S21Matrix& other);
        void operator*=(const double num);

        double operator()(int i, int j) const;


        friend void PrintTo(const S21Matrix& mtr, std::ostream* os);
        void print_matrix(std::ostream& os) const;
};

#endif  // SRC_S21_MATRIX_OOP_H_
