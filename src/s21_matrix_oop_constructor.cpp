#include <stdexcept>

#include "s21_matrix_oop.h"

//  *** Def constructor with 1x1 size
S21Matrix::S21Matrix() : S21Matrix(3, 3) {}

//  *** Construct matrix with rows x cols dimension
S21Matrix::S21Matrix(int rows, int cols) : _rows(rows), _cols(cols) {
    if (rows <= 0 || cols <= 0)
        throw std::logic_error("Incorrect size for matrix.");
    _matrix.reserve(rows * cols);
}

//  *** Copy constructor
S21Matrix::S21Matrix(const S21Matrix& other) : _rows(other._rows),
                                               _cols(other._cols),
                                               _matrix(other._matrix) {}

//  *** Move constructor
//      Is it possible to call default constructor from move constructor?
//      https://stackoverflow.com/questions/57776952/is-it-possible-to-call-default-constructor-from-move-constructor
S21Matrix::S21Matrix(S21Matrix&& other) : _rows(other._rows),
                                          _cols(other._cols),
                                          _matrix(std::move(other._matrix)) {
    other._rows = 0;
    other._cols = 0;
}

// Destructor
S21Matrix::~S21Matrix() {
    //  no need to destruct
}
