#include <fstream>
#include "s21_matrix_oop.h"

/**
 * @brief check for sizes of 2 matrices are identical
 * Matrix sizes is identical if identical if rows is identical and cols is identical.
 * @param other second matrix
 * @return true if size is identical
 * @return false if size is different
 */
bool S21Matrix::is_matrix_identical_sizes(const S21Matrix& other) const {
    return _rows == other._rows && _cols == other._cols;
}

/**
 * @brief Check if matrix is square
 * Matrix is square if its dimensions is identical (rows == cols)
 * 
 * @return true if matrix is square
 * @return false otherwise
 */
bool S21Matrix::is_square_matrix() const {
    return _rows == _cols;
}

/**
 * @brief Get matrix withot burned row and cols
 * Example: 
 * i = 1 j = 1
 * matrix:
 * 1 2 3
 * 4 5 6
 * 7 8 9
 * return matrix:
 * 1 3
 * 7 9
 * @param i row for burn
 * @param j col for burn
 * @return S21Matrix* burned matrix
 */
S21Matrix S21Matrix::matrix_without_row_col(int i, int j) {
    S21Matrix ret = S21Matrix(_rows-1, _cols-1);
    int rows_adder = 0;

    for (int k = 0; k < _rows; k++) {
        int cols_adder = 0;
        if (k == i) {
            rows_adder = 1;
            continue;
        }
        for (int m = 0; m < _cols; m++) {
            if (m == j) {
                cols_adder = 1;
                continue;
            }
            int pos = get_pos(k-rows_adder, m-cols_adder, _rows - 1);
            ret._matrix[pos] = (*this)(k, m);
        }
    }
    return ret;
}

/**
 * @brief Read matrix from file
 * 
 */
S21Matrix::S21Matrix(std::string file_name) {
    std::ifstream infile(file_name);

    int rows, cols;
    if (infile.good()) {
        infile >> rows >> cols;
        if (!infile.good()) {
            throw std::logic_error("Format exception. Rows and cols do not read.");
        }
    } else {
        throw std::logic_error(std::string("Cabbot open ") + file_name);
    }

    // in this place we certainty know what rows, cols readed ok. otherwise exception throws.
    S21Matrix ret(rows, cols);
    for (int i = 0; i < rows && !infile.fail(); i++) {
        for (int j = 0; j < cols && !infile.fail(); j++) {
            double d;
            infile >> d;
            if (!infile.good()) {
                throw std::logic_error(std::string("Format exception. Matrix not read. i = ") +
                                       std::to_string(i) +
                                        " j = " +
                                        std::to_string(j));
            }
            ret._matrix.push_back(d);
        }
    }
    infile.close();
    (*this) = ret;
}

/**
 * @brief Print matrix
 * 
 * @param os stream for print to
 */
void S21Matrix::print_matrix(std::ostream& os) const {
    for (int i = 0; i < _rows; i++) {
        for  (int j = 0; j < _cols; j++) {
            os << (*this)(i, j);
            if (j == _cols-1)
                os << "\n";
            else
                os << " ";
        }
    }
}


// cppcheck-suppress unusedFunction
void PrintTo(const S21Matrix& mtr, std::ostream* os) {
    mtr.print_matrix(*os);
}

int S21Matrix::get_pos(int i, int j) const {
    return get_pos(i, j, _cols);
}

int S21Matrix::get_pos(int i, int j, int count_rows) const {
    return i * count_rows + j;
}
